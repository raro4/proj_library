from django.db import models


# Create your models here.
class Author(models.Model):
    first_name = models.CharField(verbose_name="Имя", max_length=25)
    last_name = models.CharField(verbose_name="Фамилия", max_length=25)
    patronymic = models.CharField(verbose_name="Отчество", max_length=30, null=True)

    def __str__(self):
        return self.last_name

    class Meta:
        verbose_name = "Автор"
        verbose_name_plural = "Авторы"


class Book(models.Model):
    name = models.CharField(verbose_name="Название", max_length=50, null=True)
    amount = models.IntegerField(verbose_name="Цена")
    are_available = models.BooleanField(verbose_name="В наличии")
    author = models.ForeignKey(Author, on_delete=models.PROTECT, null=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Книга"
        verbose_name_plural = "Книги"


class Genre(models.Model):
    genre = models.CharField(verbose_name="Жанр", max_length=150)
    book = models.ForeignKey(Book, on_delete=models.PROTECT, null=True)
    author = models.ForeignKey(Author, on_delete=models.PROTECT, null=True)

    def __str__(self):
        return self.genre

    class Meta:
        verbose_name = "Жанр"
        verbose_name_plural = "Жанр"
