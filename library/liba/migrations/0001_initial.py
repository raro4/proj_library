# Generated by Django 4.0.6 on 2022-07-12 13:58

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Author',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('first_name', models.CharField(max_length=25, verbose_name='Имя')),
                ('last_name', models.CharField(max_length=25, verbose_name='Фамилия')),
                ('patronymic', models.CharField(max_length=30, null=True, verbose_name='Отчество')),
            ],
            options={
                'verbose_name': 'Автор',
                'verbose_name_plural': 'Авторы',
            },
        ),
        migrations.CreateModel(
            name='Book',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50, null=True, verbose_name='Название')),
                ('amount', models.IntegerField(verbose_name='Цена')),
                ('are_available', models.BooleanField(verbose_name='В наличии')),
                ('author', models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, to='liba.author')),
            ],
            options={
                'verbose_name': 'Книга',
                'verbose_name_plural': 'Книги',
            },
        ),
        migrations.CreateModel(
            name='Genre',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('genre', models.CharField(max_length=150, verbose_name='Жанр')),
                ('author', models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, to='liba.author')),
                ('book', models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, to='liba.book')),
            ],
            options={
                'verbose_name': 'Жанр',
                'verbose_name_plural': 'Жанр',
            },
        ),
    ]
